<?php

class Draw
{
	const FILE_NAME = 'sets.json';

	public function save($sets = array())
	{
		$existing = $this->getExistingSets();
		$result = array_merge_recursive($this->getExistingSets(), $sets);	
		
		while(!is_writable(Draw::FILE_NAME)){
			file_put_contents(Draw::FILE_NAME, json_encode($result));
		}


	}

	public function delete()
	{
		if (file_exists(Draw::FILE_NAME)) {
			unlink(Draw::FILE_NAME);
		}
	}

	public function output($n)
	{
		$results = $this->retrieveTop($n);

		return $results;
	}

	public function getExistingSets()
	{
		if (file_exists(Draw::FILE_NAME)) {
			$existing = json_decode(file_get_contents(Draw::FILE_NAME));
			return is_array($existing) ? $existing : array();
		}

		return array();
	}

	// get the top drawnNumbers that has the most drawnCounts
	// if drawnCounts are equal determine it using the priority
	// priority = 1 > priority = 2
	private function retrieveTop($n)
	{
		$parseData = $this->parseData();
		$results = array();
		$minDrawn = array();

		foreach($parseData as $drawnNumber => $drawnDetails) {

			if (count($results) < $n) {
				$results[$drawnNumber] = array_merge($drawnDetails, array('drawnNumber' => $drawnNumber));
				$minDrawn = $this->getLowestFromResults($results);
			} else {

				if ($minDrawn['drawnCount'] < $drawnDetails['drawnCount']) {

					// remove data from the selected results
					unset($results[$minDrawn['drawnNumber']]);
					$results[$drawnNumber] = array_merge($drawnDetails, array('drawnNumber' => $drawnNumber));

					$minDrawn = $this->getLowestFromResults($results);

				} elseif ($minDrawn['drawnCount'] == $drawnDetails['drawnCount'] 
					&& $minDrawn['priority'] < $drawnDetails['drawnCount'] 
					) {

					// remove data from the selected results
					unset($results[$minDrawn['drawnNumber']]);
					$results[$drawnNumber] = array_merge($drawnDetails, array('drawnNumber' => $drawnNumber));

					$minDrawn = $this->getLowestFromResults($results);

				}
			}
		}

		usort($results, function($a, $b) {
		    return $b['drawnCount'] - $a['drawnCount'];
		});
		
		return $results;

	}

	private function getLowestFromResults($results)
	{
		$minDrawn = array();

		foreach ($results as $drawnNumber => $drawnDetails) {

			if (empty($minDrawn)) {
				// fill the results with the first n from parseData
				$minDrawn = array_merge($drawnDetails, array('drawnNumber' => $drawnNumber));
				
			} else {
				// setup the lowest drawnCount or priority inside the result
				
				if ($minDrawn['drawnCount'] > $drawnDetails['drawnCount']) {
					$minDrawn = array_merge($drawnDetails, array('drawnNumber' => $drawnNumber));
				} elseif ($minDrawn['drawnCount'] == $drawnDetails['drawnCount'] 
					&& $minDrawn['priority'] < $drawnDetails['priority'] 
					) {
					$minDrawn = array_merge($drawnDetails, array('drawnNumber' => $drawnNumber));
				}
			}
		}

		return $minDrawn;
	}

	// rules for priority is like first come first serve
	private function parseData()
	{
		$results = array();
		$priority = 1;
		$sets = $this->getExistingSets();

		foreach($sets as $setsId => $set) {
			foreach($set as $setId => $val) {
				if (isset($results[$val])) {
					$results[$val]['drawnCount'] = $results[$val]['drawnCount'] + 1;
				} else {
					$results[$val] = array(
						'drawnCount' => 1,
						'priority' => $priority
					);
					$priority++;
				}
				
			}
		}

		return $results;
	}
}