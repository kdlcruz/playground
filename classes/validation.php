<?php

class Validations
{
	private $_data = null;
	private $_rules = null;
	private $_results = array();

    public function __construct($data, $rules)
    {
    	$this->setData($data);
    	$this->setRules($rules);

        $this->_validate();
    }

    public function isValid()
    {
    	return !count($this->_results);
    }

	public function setRules($rules) 
    {
    	$this->_rules = $rules;
    }

    public function getRules()
    {
    	return $this->_rules;
    }

    public function setData($data) 
    {
    	$this->_data = $data;
    }

    public function getData()
    {
    	return $this->_data;
    }

    public function setResults($results)
    {
    	$this->_results = $results;
    }

    public function getResults()
    {
    	return $this->_results;
    }

    private function _validate()
    {
    	$data = $this->getData();
    	$rules = $this->getRules();

    	$results = array();

    	foreach($data as $key => $value) {
    		if (!isset($rules[$key])) {
				continue;
    		}

    		foreach($rules[$key] as $name => $value) {
				switch ($name) {
					case 'type':
					    switch ($value) {
					    	case 'num':
					    		if (!is_numeric($data[$key])) {
									$results[$key]['error'] = 'Input must be a number!';
								}
					    	break;
					    }
						break;

					case 'min':
						if (is_numeric($data[$key]) && $data[$key] < $value) {
							$results[$key]['error'] = 'Input must be greater than ' . $value;
						}
						break;

					case 'max':
						if (is_numeric($data[$key]) && $data[$key] > $value) {
							$results[$key]['error'] = 'Input must be less than ' . $value;
						}
						break;
				}
    		}
    	}

    	$this->setResults($results);
    }
}
