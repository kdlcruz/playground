<!DOCTYPE html>
<html>
	<head>
		<title>Skill Test</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" integrity="sha384-aUGj/X2zp5rLCbBxumKTCw2Z50WgIr1vs/PFN4praOTvYXWlVyh2UtNUU0KAUhAX" crossorigin="anonymous">
		<link rel="stylesheet" href="main.css">
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3>Determine frequently drawn numbers</h>
						</div>
						<div class="panel-body">
							<form method="POST">
								<?php foreach($formFields as $name => $formData): ?>
									<div class="form-group">
										<div class="input-group input-group-lg">
											<span class="input-group-addon"><?php echo $formData['label']; ?></span>
											<input type="text" name="<?php echo $name; ?>" class="form-control" placeholder="<?php echo $formData['placeholder']; ?>">
										</div>
									</div>
								<?php endforeach; ?>
								<button type="submit" class="btn btn-primary btn-lg center-block">Submit</button>
							</form>
						</div>
					</div>
					
				</div>
			</div>

			<?php include 'templates/output.php'; ?>
			<?php include 'templates/sets.php'; ?>
		</div>

		<?php include 'templates/global-loader.php'; ?>

		<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
		<script src="main.js"></script>
	</body>
</html>