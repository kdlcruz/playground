<?php

$formFields = array(
	'possibleNumber' => array(
		'placeholder' => '1 - 100',
		'label' => 'Possible numbers'
	),
	'numbersPerSet' => array(
		'placeholder' => '1 - 10',
		'label' => 'Numbers to drawn per set'
	),
	'set' => array(
		'placeholder' => '1 - 1000',
		'label' => 'How many sets'
	),
	'output' => array(
		'placeholder' => '1 - 100',
		'label' => 'Return top n'
	)
);

include 'form.php';