<!DOCTYPE html>
<html>
  <head>
    <title>Leadership Edge</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
    <!--Let browser know website is optimized for mobile-->

    <link rel="stylesheet" href="main.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
  </head>
  <body>
    <nav class="white lighten-1" role="navigation">
      <div class="nav-wrapper container">
        <a id="logo-container" href="#" class="brand-logo">&nbsp;</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          <li><a href="sass.html">Contact Us</a></li>
        </ul>
      </div>
    </nav>

    <div id="login-content">
      <div class="container">
        <div class="row">
          <div class="col s12"><img class="right" src="http://retentionedge.dynamicresults.com/retentionedge/layouts/default/css/images/LE_Logo.jpg"></div>
        </div>
        <div class="row">
          <div class="col s4 offset-s4">
            <h1>Login</h1>
          </div>
          <form class="col s4 offset-s4" method="POST">
            <div class="row">
                <div class="input-field col s12">
                    <input id="email" name="email" type="email" class="validate" value="">
                    <label for="email" class="active">Username</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="password" name="password" type="password" class="validate" autocomplete="off">
                    <label for="password" class="active">Password</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn waves-effect waves-light light-green darken-1" type="submit" name="action">Login
                        <i class="material-icons right">send</i>
                    </button>
                </div>
                <div class="col s12">
                    <a href="#">Forgot Password</a>
                </div>
            </div>
          </form>
        </div>
        
      </div>
    </div>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
  </body>
</html>