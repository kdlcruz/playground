<!DOCTYPE html>
<html>
  <head>
    <title>Leadership Edge</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="main.css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body>
    <nav class="white lighten-1" role="navigation">
      <div class="nav-wrapper container">
        <a id="logo-container" href="#" class="brand-logo">&nbsp;</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          <li><a href="#">Contact Us</a></li>
          <li><a href="#">Logout</a></li>
          <li><a href="#">Contact Us</a></li>
        </ul>
      </div>
    </nav>

    <div id="main-content">
      <div class="container">
        <div class="row">
          <div class="col s12"><img class="right" src="http://retentionedge.dynamicresults.com/retentionedge/layouts/default/css/images/LE_Logo.jpg"></div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col s12">
              <h1>View Assessments</h1>
            </div>
            <div class="input-field col s4">
                <input id="search" name="search" type="text">
                <label for="search">Search by name, username, assessment id, etc..</label>
            </div>
            <div class="input-field col s4">
                    <select name="month_filter" class="material-select">
                        <option>With a Manager / Pre-assessment</option>
                        <option>With an Assessee / Questions</option>
                        <option>Waiting for Assessor appointment</option>
                        <option>With an Assessor / Interview Assessment</option>
                        <option>Ready for Finalization</option>
                        <option>Complete</option>
                    </select>
                    <label>Filter by:</label>
                </div>
            <div class="input-field col s2">
                <input id="StartDate" name="StartDate" type="text" class="validate datepicker">
                <label for="StartDate">From Date</label>
            </div>
            <div class="input-field col s2">
                <input id="StartDate" name="StartDate" type="text" class="validate datepicker">
                <label for="StartDate">To Date</label>
            </div>
            <table class="bordered highlight">
              <thead>
                <tr>
                    <th style="width: 100px;">Action</th>
                    <th>Assessee's Name</th>
                    <th>Created By</th>
                    <th>Assessor's Username</th>
                    <th>Date Created</th>
                    <th>Last Activity</th>
                    <th>Organization</th>
                    <th>Positon</th>
                    <th>Client</th>
                    <th>Stage</th>
                </tr>
              </thead>

              <tbody>
                <?php for($x = 0; $x < 10; $x++): ?>
                  <tr>
                    <td>
                      <a href="#" title="Edit Assessment" >
                          <i class="material-icons">mode_edit</i>
                      </a>
                      <a href="#" title="Delete Assessment" onclick="return confirm('Are you sure you want to delete this?')">
                          <i class="material-icons">delete</i>
                      </a>
                      <a href="#" title="View Assessment">
                          <i class="material-icons">pageview</i>
                      </a>
                    </td>
                    <td>Kevin Jay Dela Cruz</td>
                    <td>Webmaster</td>
                    <td>Edith Anne</td>
                    <td>11/10/2015 02:16:20 pm</td>
                    <td>11/10/2015 02:25:46 pm</td>
                    <td>Example Corp 1</td>
                    <td>VP Sales</td>
                    <td>Dynamic Results Team</td>
                    <td>Waiting for Assessor appointment</td>
                  </tr>
                <?php endfor; ?>
              </tbody>
            </table>
                  
          </div>  
        </div>
        
        
      </div>
    </div>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>

    <script>
      $('.material-select').material_select();
      $( ".datepicker" ).datepicker({
          dateFormat: 'yy-mm-dd'
      });
    </script>
  </body>
</html>