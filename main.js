(function() {
	$(document).ready(function() {

		$('form').submit(function(e) {
			$('body').addClass('loading');
			$('.form-group').removeClass('has-error');
			$('span.help-block').remove();
			$('.row.output .list-group li').remove();
			$('.row.output').addClass('hide');
			$('.row.sets .list-group li').remove();
			$('.row.sets').addClass('hide');

			e.preventDefault();

			$.post(
				'validate.php',
				$('form' ).serialize(),
				function(response) {
					var res = JSON.parse(response);

					if (!res.success) {
						$('body').removeClass('loading');
						$.each(res.result, function(name, data) {
							if (typeof data.error != 'undefined') {
								var container = $('input[name="' + name + '"]').closest('.form-group');
								container.addClass('has-error');
								container.append('<span class="help-block">' + data.error + '</span>');
							}
						});
					} else {
						processInput();
					}
				}
			);
		});

		function processInput()
		{
		
			var maxSet = 100;
			var set = $('input[name="set"]').val();

			if (set > maxSet) {
				while (set > 0) {
					if (set > maxSet) {

						process(maxSet, false);
						set -= maxSet;
					} else {

						process(set, true);
						set -= set;
					}
				}
			} else {
				process(set, true);	
			}
		}

		function process(set, isFinal)
		{
			var data = {
				'possibleNumber': $('input[name="possibleNumber"]').val(),
				'numbersPerSet': $('input[name="numbersPerSet"]').val(),
				'set': set,
			};

			$.post(
				'process.php',
				data,
				function() {
					if (isFinal) {
						
						retrieveResult();
					}
				}
			);
		}

		function retrieveResult()
		{
			var data = { 
				output: $('input[name="output"]').val()
			};

			$.post(
				'retrieve.php',
				data,		
				function(response) {
					var res = JSON.parse(response);

					if (res.top.length > 0) {
						$('.row.output h3').html('Top ' + res.top.length + ' Drawn Numbers!');

						$.each(res.top, function(id, data) {
							$('.row.output .list-group').append(
								'<li class="list-group-item">'
								+ '<span class="badge">' + data.drawnCount + '</span>'
							    + 'Drawn # - ' + data.drawnNumber
							  	+ '</li>'
							);
						});

						$('.row.output').removeClass('hide');
						$('.row.sets').removeClass('hide');

						$.each(res.sets, function(id, data) {
							$('.row.sets .list-group').append(
								'<li class="list-group-item">'
								+ data
							  	+ '</li>'
							);
						});
					} else {
						alert('Something went wrong please try again!');
					}

					$('body').removeClass('loading');
					
				}
			);
		}

	});
})();