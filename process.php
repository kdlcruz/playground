<?php

	if ($_SERVER['REQUEST_METHOD'] != 'POST') {
		echo 'something went wrong';
		exit;
	}

	include 'classes/draw.php';
	
	$possibleNumber = $_POST['possibleNumber'];
	$numberPerSet = $_POST['numbersPerSet'];
	$set = $_POST['set'];

	$results = array();

	for ($x = 0; $x < $set; $x++) {
		for($i = 0; $i < $numberPerSet; $i++) {
			$results[$x][] = rand(1, $possibleNumber);
		}
	}

	if (count($results)) {
		$draw = new Draw();

		$draw->save($results);
	}

	echo json_encode($results);die;
