<?php

	if ($_SERVER['REQUEST_METHOD'] != 'POST') {
		echo 'something went wrong';
		exit;
	}

	include 'classes/draw.php';
	
	$output = $_POST['output'];
	$draw = new Draw();

	$result = $draw->output($output);
	$sets = $draw->getExistingSets();

	echo json_encode(array(
		'top' => $result,
		'sets' => $sets
	));