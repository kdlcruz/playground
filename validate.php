<?php

	if ($_SERVER['REQUEST_METHOD'] != 'POST') {
		echo 'something went wrong';
		exit;
	}

	include 'classes/validation.php';
	include 'classes/draw.php';

	$draw = new Draw();
	$draw->delete();
	
	$postData = $_POST;

	$rules = array(
		'possibleNumber' => array(
			'type' => 'num',
			'min' => 1,
			'max' => 100
		),
		'numbersPerSet' => array(
			'type' => 'num',
			'min' => 1,
			'max' => 10
		),
		'set' => array(
			'type' => 'num',
			'min' => 1,
			'max' => 1000
		),
		'output' => array(
			'type' => 'num',
			'min' => 1,
			'max' => 100
		)
	);

	$validation = new Validations($postData, $rules);

	if (!$validation->isValid()) {
		echo json_encode(array('success' => false, 'result' => $validation->getResults()));
		exit;
	}

	echo json_encode(array('success' => true));
	

	